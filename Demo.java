import java.util.Random;
public class Demo {
    public static int n = 10;

    public static void arr()
    {
        Random rand = new Random();
        double[] array = new double[n];
        for (int i = 0; i < array.length; i++)
        {
            array[i] = rand.nextInt(100);
        }

        double max = array[0];
        double min = array[0];
        double avg = 0;
        for (int i = 0; i < array.length; i++)
        {
            if (max < array[i])
                max = array[i];
            if (min > array[i])
                min = array[i];
            avg += array[i] / array.length;
        }
        System.out.println("max = " + max);
        System.out.println("min = " + min);
        System.out.println("avg = " + avg);
    }

    public static void main(String[] args)
    {
        arr();
    }
}
